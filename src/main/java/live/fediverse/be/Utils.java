package live.fediverse.be;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import javax.net.ssl.*;
import java.io.*;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Scanner;
import java.util.Date;

public class Utils {
    public final static String PROPERTY_FIELD_NAME_HOSTNAME = "hostname";
    public final static String PROPERTY_FIELD_NAME_DATABASE = "database";
    public final static String PROPERTY_FIELD_NAME_PASSWORD = "password";
    public final static String PROPERTY_FIELD_NAME_LAST_UPDATE_MILLISECONDS = "lastUpdateMilliseconds";
    public final static String PROPERTY_FIELD_NAME_USER_UPDATED_AT_MILLISECONDS = "userUpdatedAtMilliseconds";
    public final static String PROPERTY_FIELD_NAME_USERNAME = "username";
    public final static long MINUTE = 1000 * 60;
    public final static long SECOND = 1000;

    public static String clean(String s) {
        if (s == null) {
            return null;
        }
        s = s.replaceAll("\u0000", "");
        s = s.replaceAll("\n", " ");
        s = s.trim().replaceAll(" +", " ");
        return s;
    }

    public static void main(String[] args) throws Exception {
        String tableName = "pla_host_activity";
        System.out.format("%d rows in table %s.\n", Utils.getRowCount(tableName), tableName);
    }

    public static void sleep(long milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static String getProperty(JsonElement jsonElement, String propertyName) {
        if (jsonElement == null || jsonElement.isJsonNull()) {
            return "";
        }
        JsonObject jsonObject = jsonElement.getAsJsonObject();
        if (jsonObject == null || jsonObject.isJsonNull()) {
            return "";
        }
        jsonElement = jsonObject.get(propertyName);
        if (jsonElement == null || jsonElement.isJsonNull()) {
            return "";
        }
        String string = jsonElement.getAsString();
        if (string == null) {
            return "";
        } else {
            return string;
        }
    }

    public static Connection getDatabaseConnection() throws Exception {
        Properties properties = new Properties();
        properties.load(new FileInputStream("/etc/live.fediverse.Main.properties"));
        String username = properties.getProperty(PROPERTY_FIELD_NAME_USERNAME);
        String password = properties.getProperty(PROPERTY_FIELD_NAME_PASSWORD);
        String hostname = properties.getProperty(PROPERTY_FIELD_NAME_HOSTNAME);
        String database = properties.getProperty(PROPERTY_FIELD_NAME_DATABASE);
        String url = String.format("jdbc:postgresql://%s:5432/%s?ssl=true&sslmode=require&sslfactory=org.postgresql.ssl.NonValidatingFactory", hostname, database);
        Class.forName("org.postgresql.Driver");
        return DriverManager.getConnection(url, username, password);
    }

    public static int getInt(String s) {
        try {
            return Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public static long getLong(String s) {
        try {
            return Long.parseLong(s);
        } catch (NumberFormatException e) {
        }
        return 0;
    }

    public static String postAsJson(String authorizationHeader, URL url, String json) {
        StringBuilder sb = new StringBuilder();
        HttpURLConnection urlConnection;
        InputStream inputStream = null;
        try {
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }
            };
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HostnameVerifier allHostsValid = (hostname, session) -> true;
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("Content-type", "application/json");
            urlConnection.setRequestProperty("Cache-Control", "no-cache");
            urlConnection.setUseCaches(false);
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-length", Integer.toString(json.length()));
            urlConnection.setInstanceFollowRedirects(true);
            if (authorizationHeader != null) {
                urlConnection.setRequestProperty("Authorization", authorizationHeader);
            }
            urlConnection.getOutputStream().write(json.getBytes());
            int responseCode = urlConnection.getResponseCode();
            if (responseCode != 200) {
                System.out.format("Response code %d while writing: %s", responseCode, json);
            }
            inputStream = urlConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String inputLine = bufferedReader.readLine();
            while (inputLine != null) {
                sb.append("\n");
                sb.append(inputLine);
                inputLine = bufferedReader.readLine();
            }
        } catch (Exception e) {
            System.out.format("Exception %s from URL %s.\n", e.getLocalizedMessage(), url);
            e.printStackTrace();
        } finally {
            Utils.close(inputStream);
        }
        return sb.toString();
    }


    public static String readFileToString(String fileName) throws IOException {
        StringBuilder sb = new StringBuilder();
        File file = new File(fileName);
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        String line = bufferedReader.readLine();
        String lineSeparator = System.getProperty("line.separator");
        while (line != null) {
            sb.append(line);
            sb.append(lineSeparator);
            line = bufferedReader.readLine();
        }
        close(bufferedReader);
        return sb.toString();
    }

    public static void close(Object... objects) {
        for (Object object : objects) {
            if (object != null) {
                try {
                    boolean closed = false;
                    if (object instanceof java.io.BufferedOutputStream) {
                        BufferedOutputStream bufferedOutputStream = (BufferedOutputStream) object;
                        bufferedOutputStream.close();
                        closed = true;
                    }
                    if (object instanceof java.io.StringWriter) {
                        StringWriter stringWriter = (StringWriter) object;
                        stringWriter.close();
                        closed = true;
                    }
                    if (object instanceof java.sql.Statement) {
                        Statement statement = (Statement) object;
                        statement.close();
                        closed = true;
                    }
                    if (object instanceof java.io.FileReader) {
                        FileReader fileReader = (FileReader) object;
                        fileReader.close();
                        closed = true;
                    }
                    if (object instanceof java.sql.ResultSet) {
                        ResultSet rs = (ResultSet) object;
                        rs.close();
                        closed = true;
                    }
                    if (object instanceof java.sql.PreparedStatement) {
                        PreparedStatement ps = (PreparedStatement) object;
                        ps.close();
                        closed = true;
                    }
                    if (object instanceof java.sql.Connection) {
                        Connection connection = (Connection) object;
                        connection.close();
                        closed = true;
                    }
                    if (object instanceof java.io.BufferedReader) {
                        BufferedReader br = (BufferedReader) object;
                        br.close();
                        closed = true;
                    }
                    if (object instanceof Socket) {
                        Socket socket = (Socket) object;
                        socket.close();
                        closed = true;
                    }
                    if (object instanceof PrintStream) {
                        PrintStream printStream = (PrintStream) object;
                        printStream.close();
                        closed = true;
                    }
                    if (object instanceof ServerSocket) {
                        ServerSocket serverSocket = (ServerSocket) object;
                        serverSocket.close();
                        closed = true;
                    }
                    if (object instanceof Scanner) {
                        Scanner scanner = (Scanner) object;
                        scanner.close();
                        closed = true;
                    }
                    if (object instanceof InputStream) {
                        InputStream inputStream = (InputStream) object;
                        inputStream.close();
                        closed = true;
                    }
                    if (object instanceof Socket) {
                        Socket socket = (Socket) object;
                        socket.close();
                        closed = true;
                    }
                    if (object instanceof PrintWriter) {
                        PrintWriter pw = (PrintWriter) object;
                        pw.close();
                        closed = true;
                    }
                    if (!closed) {
                        String msg = "Object not closed. Object type not defined in this close method. " + object.getClass().getName()
                                + " class stack: "
                                + getClassNames();
                        System.out.println(msg);
                    }
                } catch (Exception e) {
                }
            }
        }
    }

    public static String getClassNames() {
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        StringBuilder classNames = new StringBuilder();
        for (StackTraceElement e : stackTraceElements) {
            classNames.append(e.getClassName()).append(", ");
        }
        if (classNames.toString().endsWith(", ")) {
            classNames.delete(classNames.length() - 2, classNames.length());
        }
        return classNames.toString();
    }

    public static String trim(String string) {
        if (string == null) {
            return "";
        }
        return string.trim();
    }

    public static boolean isNotBlank(String s) {
        return !isBlank(s);
    }

    public static boolean isBlank(ArrayList arrayList) {
        return (arrayList == null || arrayList.size() == 0);
    }

    public static boolean isBlank(BigDecimal bigDecimal) {
        return (bigDecimal == null || bigDecimal.compareTo(BigDecimal.ZERO) == 0);
    }

    public static boolean isBlank(int[] intArray) {
        return (intArray == null || intArray.length == 0);
    }

    public static boolean isBlank(String string) {
        return (string == null || string.trim().length() == 0);
    }

    public static boolean isBlank(String[] stringArray) {
        return (stringArray == null || stringArray.length == 0);
    }

    public static boolean isBlank(StringBuilder sb) {
        return (sb == null || sb.toString().trim().length() == 0);
    }

    public static boolean isBlank(StringBuffer sb) {
        return (sb == null || sb.toString().trim().length() == 0);
    }

    public static long getLong(Timestamp timestamp) {
        if (timestamp != null) {
            return timestamp.getTime();
        }
        return 0;
    }

    public static long getRowCount(String tableName) {
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = Utils.getDatabaseConnection();
            ps = connection.prepareStatement("SELECT reltuples::bigint AS estimate FROM pg_class where relname=?");
            ps.setString(1, tableName);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getLong("estimate");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            Utils.close(rs, ps, connection);
        }
        return 0;
    }

    public static String getFullDateAndTime(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd, yyyy hh:mm:ss a");
        return sdf.format(date);
    }

    public static String getFullDateAndTime() {
        return getFullDateAndTime(new Date());
    }

    public static String getFullDateAndTime(long milliseconds) {
        return getFullDateAndTime(new Date(milliseconds));
    }


    public static JsonObject transfer(ResultSet rs) {
        JsonArray jsonArrayRows = new JsonArray();
        JsonArray jsonArrayCols = new JsonArray();
        try {
            ResultSetMetaData rsmd = rs.getMetaData();
            int columnCount = rsmd.getColumnCount();
            for (int i = 1; i < columnCount + 1; i++) {
                String columnName = rsmd.getColumnLabel(i);
                JsonObject jsonObjectColumn = new JsonObject();
                jsonObjectColumn.addProperty("name", columnName);
                jsonObjectColumn.addProperty("type", rsmd.getColumnTypeName(i));
                jsonObjectColumn.addProperty("scale", rsmd.getScale(i));
                jsonObjectColumn.addProperty("precision", rsmd.getPrecision(i));
                jsonArrayCols.add(jsonObjectColumn);
            }
            while (rs.next()) {
                JsonObject jsonObjectRow = new JsonObject();
                for (int i = 1; i < columnCount + 1; i++) {
                    String columnName = rsmd.getColumnName(i);
                    if (rsmd.getColumnType(i) == java.sql.Types.ARRAY) {
                        jsonObjectRow.addProperty(columnName, rs.getObject(i).toString());
                    } else if (rsmd.getColumnType(i) == java.sql.Types.BIGINT) {
                        jsonObjectRow.addProperty(columnName, rs.getLong(i));
                    } else if (rsmd.getColumnType(i) == java.sql.Types.BOOLEAN) {
                        jsonObjectRow.addProperty(columnName, Boolean.toString(rs.getBoolean(i)));
                    } else if (rsmd.getColumnType(i) == java.sql.Types.BLOB) {
                        jsonObjectRow.addProperty(columnName, "Blob column type not supported");
                    } else if (rsmd.getColumnType(i) == java.sql.Types.CHAR) {
                        jsonObjectRow.addProperty(columnName, rs.getString(i));
                    } else if (rsmd.getColumnType(i) == java.sql.Types.DOUBLE) {
                        jsonObjectRow.addProperty(columnName, rs.getDouble(i));
                    } else if (rsmd.getColumnType(i) == java.sql.Types.DECIMAL) {
                        jsonObjectRow.addProperty(columnName, rs.getDouble(i));
                    } else if (rsmd.getColumnType(i) == java.sql.Types.FLOAT) {
                        jsonObjectRow.addProperty(columnName, rs.getFloat(i));
                    } else if (rsmd.getColumnType(i) == java.sql.Types.INTEGER) {
                        jsonObjectRow.addProperty(columnName, rs.getInt(i));
                    } else if (rsmd.getColumnType(i) == java.sql.Types.NVARCHAR) {
                        jsonObjectRow.addProperty(columnName, rs.getNString(i));
                    } else if (rsmd.getColumnType(i) == java.sql.Types.VARCHAR) {
                        String value = rs.getString(i);
                        if (value == null) {
                            jsonObjectRow.addProperty(columnName, "");
                        } else {
                            jsonObjectRow.addProperty(columnName, value);
                        }
                    } else if (rsmd.getColumnType(i) == java.sql.Types.TINYINT) {
                        jsonObjectRow.addProperty(columnName, rs.getInt(i));
                    } else if (rsmd.getColumnType(i) == java.sql.Types.SMALLINT) {
                        jsonObjectRow.addProperty(columnName, rs.getInt(i));
                    } else if (rsmd.getColumnType(i) == java.sql.Types.BIT) {
                        jsonObjectRow.addProperty(columnName, rs.getString(i));
                    } else if (rsmd.getColumnType(i) == java.sql.Types.NUMERIC) {
                        jsonObjectRow.addProperty(columnName, rs.getDouble(i));
                    } else if (rsmd.getColumnType(i) == java.sql.Types.DATE) {
                        jsonObjectRow.addProperty(columnName, rs.getString(i));
                    } else if (rsmd.getColumnType(i) == java.sql.Types.TIMESTAMP) {
                        jsonObjectRow.addProperty(columnName, rs.getString(i));
                    } else if ("bool".equals(rsmd.getColumnTypeName(i))) {
                        jsonObjectRow.addProperty(columnName, Boolean.toString(rs.getBoolean(i)));
                    } else if ("jsonb".equals(rsmd.getColumnTypeName(i))) {
                        jsonObjectRow.addProperty(columnName, rs.getString(i));
                    } else {
                        Object object = rs.getObject(i);
                        if (object instanceof String) {
                            String value = rs.getString(i);
                            if (value == null) {
                                jsonObjectRow.addProperty(columnName, "");
                            } else {
                                jsonObjectRow.addProperty(columnName, value);
                            }
                        } else {
                            System.out.format("Column %s is an unknown type. Type: %s Object: %s\n", columnName, rsmd.getColumnTypeName(i),
                                    object);
                            jsonObjectRow.addProperty(columnName, (String) rs.getObject(i));
                        }
                    }

                }
                jsonArrayRows.add(jsonObjectRow);
            }
        } catch (Exception e) {
            e.printStackTrace();
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("message", e.getLocalizedMessage());
            return jsonObject;
        } finally {
            Utils.close(rs);
        }
        JsonObject jsonObjectResponse = new JsonObject();
        jsonObjectResponse.addProperty("quantityRows", jsonArrayRows.size());
        jsonObjectResponse.addProperty("quantityCols", jsonArrayCols.size());
        long responseDateMilliseconds = System.currentTimeMillis();
        jsonObjectResponse.addProperty("responseDateMilliseconds", responseDateMilliseconds);
        jsonObjectResponse.addProperty("responseDateDisplay", Utils.getFullDateAndTime(responseDateMilliseconds));
        jsonObjectResponse.add("rows", jsonArrayRows);
        jsonObjectResponse.add("cols", jsonArrayCols);
        return jsonObjectResponse;
    }

}

