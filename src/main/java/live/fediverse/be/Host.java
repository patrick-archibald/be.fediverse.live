package live.fediverse.be;

public class Host {
    private String name;
    private int id;
    private long lastPostMilliseconds;
    private String lastPostDisplay;
    private int httpResponseCode;
    private String httpGetContent;
    private String exceptionText;
    private int quantityDomain;
    private int quantityStatus;
    private int quantityUser;
    private String title;
    private String description;
    private boolean registrationOpen = false;
    private String version;
    private long insertedAtMilliseconds;
    private String insertedAtDisplay;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getLastPostMilliseconds() {
        return lastPostMilliseconds;
    }

    public void setLastPostMilliseconds(long lastPostMilliseconds) {
        this.lastPostMilliseconds = lastPostMilliseconds;
    }

    public String getLastPostDisplay() {
        return lastPostDisplay;
    }

    public void setLastPostDisplay(String lastPostDisplay) {
        this.lastPostDisplay = lastPostDisplay;
    }

    public int getHttpResponseCode() {
        return httpResponseCode;
    }

    public void setHttpResponseCode(int httpResponseCode) {
        this.httpResponseCode = httpResponseCode;
    }

    public String getHttpGetContent() {
        return httpGetContent;
    }

    public void setHttpGetContent(String httpGetContent) {
        this.httpGetContent = httpGetContent;
    }

    public String getExceptionText() {
        return exceptionText;
    }

    public void setExceptionText(String exceptionText) {
        this.exceptionText = exceptionText;
    }

    public int getQuantityDomain() {
        return quantityDomain;
    }

    public void setQuantityDomain(int quantityDomain) {
        this.quantityDomain = quantityDomain;
    }

    public int getQuantityStatus() {
        return quantityStatus;
    }

    public void setQuantityStatus(int quantityStatus) {
        this.quantityStatus = quantityStatus;
    }

    public int getQuantityUser() {
        return quantityUser;
    }

    public void setQuantityUser(int quantityUser) {
        this.quantityUser = quantityUser;
    }



    public void setTitle(String title) {
        this.title = title;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isRegistrationOpen() {
        return registrationOpen;
    }

    public void setRegistrationOpen(boolean registrationOpen) {
        this.registrationOpen = registrationOpen;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public long getInsertedAtMilliseconds() {
        return insertedAtMilliseconds;
    }

    public void setInsertedAtMilliseconds(long insertedAtMilliseconds) {
        this.insertedAtMilliseconds = insertedAtMilliseconds;
    }

    public String getInsertedAtDisplay() {
        return insertedAtDisplay;
    }

    public void setInsertedAtDisplay(String insertedAtDisplay) {
        this.insertedAtDisplay = insertedAtDisplay;
    }


}
