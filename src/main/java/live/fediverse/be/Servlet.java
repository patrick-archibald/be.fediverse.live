package live.fediverse.be;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

public class Servlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doPost(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        log(String.format("Action: %s Date: %s\n", action, new Date()));
        Gson gson;
        if (request.getParameter("debug") != null) {
            gson = new GsonBuilder().setPrettyPrinting().create();
        } else {
            gson = new Gson();
        }
        PrintWriter pw = new PrintWriter(response.getOutputStream());
        response.setContentType("application/json");
        if ("hosts".equals(action)) hosts(request, response, gson, pw);
        pw.flush();
        pw.close();
    }

    private void hosts(HttpServletRequest request, HttpServletResponse response, Gson gson, PrintWriter pw) throws ServletException, IOException {
        HostDAO dao = new HostDAO();
        boolean allFields = false;
        boolean dead = (request.getParameter("dead") != null);
        gson.toJson(dao.get(allFields, dead), pw);
    }
}
