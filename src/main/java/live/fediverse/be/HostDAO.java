package live.fediverse.be;

import java.sql.*;
import java.util.ArrayList;

public class HostDAO {
    public static void main(String[] args) {
        HostDAO dao = new HostDAO();
        boolean allFields = false;
        boolean dead = true;
        ArrayList<Host> hosts = dao.get(allFields, dead);
        for (Host host : hosts) {
            System.out.format("%d\t%d\t%d\t%s\t%s\n",
                    host.getQuantityDomain(), host.getQuantityStatus(), host.getQuantityUser(), host.getName(), host.getLastPostDisplay());
        }
        System.out.format("%d hosts\n", hosts.size());
    }

    public ArrayList<Host> get(boolean allFields, boolean dead) {
        ArrayList<Host> hosts = new ArrayList<Host>();
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sqlStatement = "select * from pla_host_activity";
        if (dead) {
            sqlStatement = sqlStatement.concat(" where last_post is null or last_post < current_timestamp - interval '12 days'");
        } else {
            sqlStatement = sqlStatement.concat(" where last_post >= current_timestamp - interval '12 days'");
        }
        try {
            connection = Utils.getDatabaseConnection();
            ps = connection.prepareStatement(sqlStatement);
            rs = ps.executeQuery();
            while (rs.next()) {
                hosts.add(transfer(rs, allFields));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hosts;
    }
    private Host transfer(ResultSet rs, boolean allFields) throws SQLException {
        Host host = new Host();
        host.setId(rs.getInt("id"));
        host.setName(rs.getString("host"));
        host.setQuantityDomain(rs.getInt("domain_count"));
        host.setQuantityStatus(rs.getInt("status_count"));
        host.setQuantityUser(rs.getInt("user_count"));
        host.setTitle(rs.getString("title"));
        host.setDescription(rs.getString("description"));
        host.setVersion(rs.getString("version"));
        if (allFields) {
            host.setExceptionText(rs.getString("exception_text"));
            host.setHttpGetContent(rs.getString("http_get_content"));
        }
        Timestamp timestamp = rs.getTimestamp("last_post");
        host.setLastPostMilliseconds(Utils.getLong(timestamp));
        host.setLastPostDisplay(Utils.getFullDateAndTime(timestamp));
        return host;
    }
}
